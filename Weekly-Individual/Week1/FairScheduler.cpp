/* Week 1 Individual Coding Assingment
* *  Problem 1 
* *  Abigail Greentree, agreentr@nd.edu
* *  Completely Fair Scheduler implemented with a Red-Black tree
* * March 24th, 2019
*/

#include "RBTree.h"
#include <string>
using namespace std;

struct process {
	int procNum;
	int vruntime;
	
	process(int pN, int vRT):procNum(pN), vruntime(vRT){};

	void setValues(int pN, int vRT){
		procNum=pN;
		vruntime=vRT; 
	}

	//Overload operator functions
	bool operator>(const process& proc2) const{
		if(vruntime > proc2.vruntime)
			return true; 
		else
			return false;
	}

	bool operator<(const process& proc2) const{
		if(vruntime < proc2.vruntime)
			return true;
		else
			return false;
	}

	bool operator==(const process& proc2) const{
		if(vruntime == proc2.vruntime)
			return true;
		else 
			return false;
	}
	
	friend ostream& operator<<(ostream& os, const process& proc);
};
	
ostream& operator<<(ostream& os, const process& proc){
	os <<"{"<<proc.procNum<<", "<<proc.vruntime<<"}"<< endl;
	return os; 
}

int main(int argc, char** argv){
	//Declare and Initialize Variables
	int i=0;
	RBTree<process> FSTree; 

	int v[10]={4,17,9,35,20,29,2,31,12,9};
	
	
	//Create processes and insert into the RBTree
	for(i=0; i<10; i++){
		process value(i+1, v[i]); 
		FSTree.insert(value); 
	}
	
	FSTree.printInOrder();
	// Delete Leftmost Node
	process value(7,2);
	FSTree.deleteByVal(value);

	FSTree.printInOrder();

	return 0;
}



/* Weekly Coding Assignment 2
 * Problem 2: Design a method to find the frequency of occurences of any given word in a book. 
 * Abigail Greentree
 * agreentr@nd.edu
 * March 31st, 2019
*/

// Include Libraries
#include <fstream>
#include <iostream> 
#include <string> 
#include <map>
#include <unordered_map>
#include <iterator>

using namespace std;

// Function Prototypes
ifstream & openFile(ifstream& ifs, char* fileName);
bool checkPunctuation(string& word, string& checkWord);

//Main
int main(int argc, char* argv[]){
	// Declare and Initialize Values
	ifstream ifs; 
	string word; 
	string checkWord;
	int freq;
	int wordSize; 
	int checkWordSize;
	char search = 'Y';
	map<string, int> wordMap;
	map<string, int>::iterator iter;

	while (search == 'Y'){

		//Reset Neccessary Variables
		openFile(ifs, argv[1]);
		freq = 0;

		//Get User Word
		cout << "Enter the word you want to count the frequency of in all lowercase letters: "; 
		cin>>word; 
        	wordSize = word.length();	
		
		//Check if word is already in the Hash Map
		iter = wordMap.find(word);
		if(iter != wordMap.end()){
			cout << "You have searched for this word before." << endl;
			freq = wordMap[word]; 
		}
		else { //if it is not in the Hash Map, check that its in the file
			while(! ifs.eof()){ 
				ifs >> checkWord; //get the next word from the input file
				
				if(isupper(checkWord.front())){ //ensure the first char of checkWord is lower case
					checkWord[0]=checkWord[0]+32; 
				}	
				
				//Compare each word in file to the user inputted word
				if(word.compare(checkWord)==0){ //Check if the two strings are the same
					freq++; //increment frequency
				}
				else if(word.compare(0, wordSize, checkWord, 0, wordSize)==0){ //check if checkWord contains the word even if it is not exactly the same
					if (checkPunctuation(word, checkWord)) // check for the case where the checkWord ends in a punctuation mark
						freq++;
				}
			}
			wordMap[word]=freq; // Add this word to the Hash Table
		}
		
		cout << "The word '" << word << "' appears " << freq << " time(s)." << endl; //Output the result to the user

		ifs.close(); //Close filestream

		// Check if User wants to keep searching
		cout << "Would you like to search for another word (Y or N)? ";
		cin >> search;
	
	}
	

	return 0; 
}

ifstream & openFile(ifstream & ifs, char* fileName){ //File Open Function pulled from Project1
	ifs.open(fileName);
	if(!ifs){
		cout << "Invalid File" << endl; 
	}
	return ifs;
}

bool checkPunctuation(string& word, string& checkWord){	
	//Declare and Initialize Variables
	char punctuation[7]={'.', ',', '?', '!', ':', ';', '-'}; //Chosen punctuation that I think would appear in a book
	int wordSize = word.length();
	int checkWordSize = checkWord.length();
	char lastChar = checkWord.back(); //get last char of checkword, punctuation would appear on last char
	int i = 0; //iteration

	if (checkWordSize == (wordSize+1)) { //if check word is only one char bigger that world
		for(i=0; i<7; i++){
			if (lastChar == punctuation[i]){ //and that char is a punctuation mark
			       return true;  //count as an instance of the word
			}	       
		}
	}
	return false; 
}

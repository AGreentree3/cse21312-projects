/************************
 * SortedArray.cpp
 * Author: Abigail Greentree
 * Email: agreentr@nd.edu
 * 
 * Code for problem 1 of the Week 4 Individual Coding Assignment. 
 * Problem Statement: Given a sorted (increasing order) array with unique integer elements, write an algorithm to create a binary search tree with minimal height.
*************************/

//Include Libraries
#include <iostream>
#include <algorithm>
#include "BST.h" //Using the BSTree Data Structure provided by the instructor in the InClassWork Repository

//Function Prototypes
void printArray(int myArr[], int start, int size);
void findMiddle(int myArr[], int start, int size);

//Global Variables
BST<int>* intBST = new BST<int>();

int main(void){
	//Declare and Initialize Variables
	int intArray[15] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}; //Create integer array to insert into the BSTree
	int i=0; //iterator

	std::cout<<"Sorted Integer Array: ";
	printArray(intArray, 0,  15);
	std::cout<<"Insertion Order: ";
	findMiddle(intArray, 0, 15); // Use the algorithm to find the best insertion order
	std::cout<<std::endl;

	std::cout<<"BST elements: " <<*intBST<<std::endl; //Print the tree
	return 0;
}

void printArray(int myArr[], int start, int size){
	int i=0;
	for(i=start; i<size; i++){
		std::cout<<myArr[i]<<" ";
	}
	std::cout<<std::endl;
}

void findMiddle(int myArr[], int start, int size){
	if((size-start)<1){
		//Do Nothing, this is the base case.	
	}
	else{
		int middleIndex = start + (size-start) / 2; // Find the middle value
		intBST->insert(myArr[middleIndex]); //insert it into the BSTree
		std::cout << myArr[middleIndex]<<", "; //print it for the user
		findMiddle(myArr, start, middleIndex);  //recursive call for the left subarray
		findMiddle(myArr, middleIndex+1, size); //recursive call for the right subarray
	} 
}

/************************
 * RoadTrip.cpp
 * Cadence 4 Assignment
 * Abigail Greentree
 * agreentr@nd.edu
 * 4-15-19
 * **********************/

//Include Libraries
#include <fstream>
#include <iostream> 
#include <cstdlib> 
#include <ctime> 
#include <string>
#include "SeparateChaining.h" //  Using instructor's seperate chaining hash table with some modifications as the base data structure. 
using namespace std;

ifstream & openFile(ifstream& ifs, char* fileName);

int main(int argc, char *argv[]){
	//Declare and Initialize Variables
	HashTable<string> hTable(1000);
	ifstream ifs;
	openFile(ifs, argv[1]);
	string hashWord;
	string plateNumber;
	
	//Import dictionary file and store it in the hashtable
	while(! ifs.eof()){
		ifs >> hashWord;
		hTable.insert(hashWord);
	}
	ifs.close();
	//hTable.printHash(cout);
	
	//Get the license plate the user wants to check
	cout << "Enter the license plate: ";
	cin >> plateNumber;
	
	//Remove numbers from the license plate
	for (int i=0; i<plateNumber.length(); i++){
		if ((plateNumber[i]<65) || (plateNumber[i]>90)) {
			plateNumber.erase(i,1);
			i = i-1;
		}
	}
	//cout << plateNumber << endl;

	//Check if the letters in the string make a word
	if (hTable.contains(plateNumber)){
		//Do Nothing
	}
	else { //If not add on letters
		char appendChar = 'A';
		plateNumber += appendChar;
		while(!hTable.contains(plateNumber)){
			plateNumber.pop_back();
			appendChar++;
			plateNumber += appendChar;
		}
	}	
	
	return 0; 
}

ifstream & openFile(ifstream & ifs, char* fileName){ //File Open Function pulled from Project1
	ifs.open(fileName);
	if(!ifs){
		cout << "Invalid File" << endl;
	}
	return ifs;
}

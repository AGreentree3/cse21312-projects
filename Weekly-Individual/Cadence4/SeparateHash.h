/**********************************************
* File: SeparateHash.h 
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/

#ifndef SEPARATEHASH_H
#define SEPARATEHASH_H 

#include <iostream>
//using namespace std;

/********************************************
* Function Name  : isPrime
* Pre-conditions :  int n 
* Post-conditions: bool
* 
* Internal method to test if a positive number is prime.
* Not an efficient algorithm.
********************************************/
bool isPrime( int n )
{
    if( n == 2 || n == 3 )
        return true;

    if( n == 1 || n % 2 == 0 )
        return false;

    for( int i = 3; i * i <= n; i += 2 )
        if( n % i == 0 )
            return false;

    return true;
}

/********************************************
* Function Name  : nextPrime
* Pre-conditions :  int n 
* Post-conditions: int
*
* Internal method to return a prime number at least as large as n.
* Assumes n > 0.
********************************************/
int nextPrime( int n )
{
    if( n % 2 == 0 )
        ++n;

    for( ; !isPrime( n ); n += 2 )
        ;

    return n;
}

/********************************************
* Function Name  : hash
* Pre-conditions :  const string & key 
* Post-conditions: size_t
*  
* A hash routine for string objects.
********************************************/
long hash( const std::string & key )
{
	int i=0; 
	long prod = 1;  
	for (i=0; i<key.length(); i++){
		prod *= ((int)key[i]-63); 
	}
    return prod;
}

/********************************************
* Function Name  : hash
* Pre-conditions :  int key 
* Post-conditions: size_t
* 
* A hash routine for ints.
********************************************/
size_t hash( int key )
{
    return key;
}

#endif

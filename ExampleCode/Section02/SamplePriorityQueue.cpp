/****************************************
 * File name: SimplePriorityQueue.cpp 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the main driver function 
 * to show a simple Priority Queue implementation
 * *************************************/

#include <iostream> 
#include <stdlib.h>  // itoa

#include "PriorityQueue.h"
#include "SimpleProcess.h"

size_t AGING = 5;

/*********************************************
* Function Name: aging
* Preconditions: PriorityQueue<SimpleProcess>&  
* Postconditions: void 
* This function subtracts AGING from the priority 
* value, as the piority is lower values.
* ******************************************/ 
void aging(PriorityQueue<SimpleProcess>& pQueueEx){
    
    PriorityQueue<SimpleProcess> temp;
    
    while(pQueueEx.size() > 0){
        SimpleProcess tempProcess = pQueueEx.top();
        tempProcess.ageProcess(AGING);
        temp.emplace(tempProcess);
        pQueueEx.pop();
    }
    
    while(temp.size() > 0){
        SimpleProcess tempProcess = temp.top();
        pQueueEx.emplace(tempProcess);
        temp.pop();
    }
    
}

/*********************************************
* Function Name: main
* Preconditions: int argc, char** argv 
* Postconditions: int 
* This main driver function creates four SimpleProcess 
* objects, puts them into the queue with an emplace 
* object, and then dequeues them in the priority order
* ******************************************/ 
int main (int argc, char** argv) 
{ 
    SimpleProcess worst;
    SimpleProcess pLowest("Lowest Priority Print String");
    SimpleProcess pLow("Low Priority Print String");
    SimpleProcess pMed("Medium Priority");
    SimpleProcess pHigh("High Pr");
    SimpleProcess pHighest("W");
    
    std::cout << worst << std::endl;
    std::cout << pLowest << std::endl;
    std::cout << pLow << std::endl;
    std::cout << pMed << std::endl;
    std::cout << pHigh << std::endl;
    std::cout << pHighest << std::endl << std::endl;
    
    PriorityQueue<SimpleProcess> pQueueEx;
    
    pQueueEx.emplace(pLow);
    pQueueEx.emplace(pHigh);
    pQueueEx.emplace(pMed);
    pQueueEx.emplace(pHighest);
    pQueueEx.emplace(worst);
    pQueueEx.emplace(pLowest);
  
    while(pQueueEx.size() > 0){
  
        std::cout << "pQueueEx.size() : " << pQueueEx.size(); 
        std::cout << ", pQueueEx.top() : " << pQueueEx.top() << std::endl;
    
        pQueueEx.pop(); 
        
        // Age the elements
        aging(pQueueEx);
      
        SimpleProcess tempProcess =  pQueueEx.top();
      
        if(pQueueEx.size() % 3 == 0 && tempProcess.getPriority() > AGING){
            // Create new Input with a string with "High " and the size of pQueueEx
            SimpleProcess pNew("High");
            pQueueEx.emplace(pNew);
        }
    }
  
    return 0; 
} 